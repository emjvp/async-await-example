'use_strict';
let api_btn = document.getElementById("btn_api");

api_btn.addEventListener("click", ()=>{
    let date = document.getElementById("calendar").value;

    getApiExchange(date).then(        
        result => {             
            document.getElementById("mxrate").innerHTML = result.rates.MXN;
        }).catch(err => {
        console.log(err);
    });
});

