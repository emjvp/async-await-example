
let getApiExchange = async (date) =>{
    
    let result = undefined;
   
        await $.get(`https://api.exchangeratesapi.io/${date}`, (data)=>{
            result = data;  
            
        }); 
        if(result){               
            return result;
        }else{
            throw new Error("you had a problem");
        }                 
};
